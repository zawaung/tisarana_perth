$(document).ready(function() {
	$('#searchbox')
		.focus(
			function() {
				if ("enter search" == $(this).val().trim().toLowerCase()) {
					$(this).val("");
				}
			}
		)
		.focusout(
			function() {
				if ("" == $(this).val().trim().toLowerCase()) {
					$(this).val("enter search");
				}
			}
		);

	$(document).ready(function() {
		$('.more')
			.click(
				function() {
					id = $(this).data("id");
					$('#summary-' + id).hide();
					$('#details-' + id).show();
				}
			);

		$('.less')
			.click(
				function() {
					id = $(this).data("id");
					$('#details-' + id).hide();
					$('#summary-' + id).show();
				}
			);
	});
});
