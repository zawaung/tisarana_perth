<?php

$activeMenu = 'news';
$action = '';

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET' : $action = filter_input(INPUT_GET, 'a', FILTER_SANITIZE_STRING); break;
}

switch ($action) {
    default : include_once('list.php'); break;
}

?>
