<?php

$dir = dirname(__FILE__);
include_once($dir . '/../../conf.php');

$page = filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT);
$pageSize = filter_input(INPUT_GET, 'pageSize', FILTER_VALIDATE_INT);

$ch = curl_init();
$options = array(CURLOPT_URL => WS_URL . "/events/?page=$page&pageSize=$pageSize", 
                    CURLOPT_RETURNTRANSFER => true);

curl_setopt_array($ch, $options);

$response = curl_exec($ch);
$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

curl_close($ch);

$a = json_decode($response, true);

if ($a && count($a) > 0 && !isset($a['error'])) {
    $page = $a['page'] + 1;
    $pageSize = $pageSize;
    $totalPage = $a['totalPage'];
    $event_list = $a['result'];
} 
else {
    $page = $a['page'] + 1;
    $pageSize = $pageSize;
    $totalPage = $a['totalPage'];
    $event_list = $a['result'];
}

unset($a);
unset($response);

include($dir . '/../../template/event/list.php');

?>
