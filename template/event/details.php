<?php 

$dir = dirname(__FILE__);

include_once($dir . '/../head.php');
include_once($dir . '/../mid.php');
?>
<form method='post'>
    <input type='hidden' name='a' value='<?php echo ($blank?"add":"update"); ?>' />
    <table style="width:100%">
        <tr>
            <td>
               ID 
            </td>
            <td>
                <input type='textbox' value='<?php echo ($blank?"":$event["id"]); ?>' name='id' />
            </td>
        </tr>
        <tr>
            <td>
               Created On 
            </td>
            <td>
                <input type='textbox' value='<?php echo ($blank?"":$event["createDate"]); ?>' name='create_date' />
            </td>
        </tr>
        <tr>
            <td>
               Title 
            </td>
            <td>
                <input type='textbox' value='<?php echo ($blank?"":$event["title"]); ?>' name='title' />
            </td>
        </tr>
        <tr>
            <td>
               Details 
            </td>
            <td>
                <input type='textbox' value='<?php echo ($blank?"":$event["details"]); ?>' name='details' />
            </td>
        </tr>
        <tr>
            <td>
                <input type='submit' value='<?php echo ($blank?"Save":"Update"); ?>' />
            </td>
        </tr>
    </table>
</form>
<?php
