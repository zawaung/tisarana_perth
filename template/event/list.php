<?php 

$dir = dirname(__FILE__);

include_once($dir . '/../head.php');
include_once($dir . '/../mid.php');

$summaryLength = 500;

if ($event_list) {
    foreach ($event_list as $event) {
?>
<div>
    <div class='event-title'>
        <?= htmlentities($event['title']); ?>
    </div>
    <div id='summary-<?= $event["id"]; ?>' class='event-summary'>
        <?php
            $isLong = (mb_strlen($event['details']) > $summaryLength);
            if ($isLong) {
                echo htmlentities(mb_substr($event['details'], 0, $summaryLength) . '.....'); 
            } 
            else {
                echo htmlentities($event['details']); 
            }
        ?>
        <div style='text-align: right;'>
        <?php
        if ($isLong)            
            echo "<input class='more' type='button' value='More' data-id='" . $event["id"] . "'/>";
        else
            echo "&nbsp;";
        ?>
        </div>
    </div>
    <div id='details-<?= $event["id"]; ?>' class='event-details'>
        <?= $event['details']; ?>
        <div style='text-align: right;'>
            <input class='less' type='button' value='Less' data-id='<?= $event["id"]; ?>' />
        </div>
    </div>
</div>
<?php
    }
}

include_once($dir . '/../foot.php');
?>
