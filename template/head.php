<html>
<head>
    <title>Tisarana Vihara Monastery - Perth - Western Australia</title>
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <meta content='text/html; charset=UTF-8' http-equiv='Content-Type'/>
    <style>
        @import "/css/main.css" screen,print;
        @font-face {
            font-family: 'Gudea-Regular';
            src: url('/font/Gudea-Regular.otf') format('opentype');
        }
        @font-face {
            font-family: 'Gudea-Bold';
            src: url('/font/Gudea-Bold.otf') format('opentype');
        }
        @font-face {
            font-family: 'Zawgyi-One';
            src: url('/font/Zawgyi-One.ttf') format('truetype');
        }
    </style>
    <script src='/javascript/jquery-1.11.1.js' type='application/javascript'></script>
    <script src='/javascript/event_handler.js' type='application/javascript'></script>
</head>
<body>
    <div id="header">
        <span class='title1'>
            <a href='/'><img src='/image/logo-80x80.png' /></a>
            <img src='/image/tisarana-title.png' />
        </span>
        <span class='title2'>
            <div>      
                Tisarana is founded by 
            </div>
            <div>
                Venerable Abbott of Myaung Mya;
            </div> 
            <div>
                ASHIN NYANIKA.
            </div>
        </span>
	</div><!-- header -->

