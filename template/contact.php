<?php 

$dir = dirname(__FILE__);

include_once($dir . '/head.php');
include_once($dir . '/mid.php');
?>
<div id="contact" style="float:left;width:400px;height:420px;">
    <div style='font-family: Zawgyi-One'>
        <strong>ဆက္သြယ္ရန္</strong><br/>
        <table>
            <tr>
                <td><em>ပင္မ</em> : ၀၈ - ၆၄၆၅ ၄၇၄၁ <em>ဦး သုဇန</em> : ၀၄၂၁ ၁၉၈ ၅၅၇</td>
            </tr>
        </table>
    </div>
    <br/>
    <div>
        <strong>Contact - Phone</strong><br/>
        <table>
            <tr>
                <td>Main : 08-6465 4714; Ven. U Sujana : 0421 198 557</td>
            </tr>
        </table>
        <br/>
        <strong>Location</strong><br/>
        21 Ashby Close<br/>
        Forrestfield WA 6058<br/>
        <br/><span><a href="https://www.facebook.com/tisarana.monastery">Tisarana.Monastery@Facebook</a></span><br/>
        <br/>Please contact a monk before visiting.<br/>
    </div>
</div>
<div id="map" style="float:right;width: 550px;height:400px;margin:2px 2px 20px 20px;border: 2px solid white">
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3384.8000037697843!2d116.001335!3d-31.966322!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2a32b8d34878e521%3A0x41923694689f9adc!2s21+Ashby+Close%2C+High+Wycombe+WA+6057!5e0!3m2!1sen!2sau!4v1409582635640" width="550" height="400" frameborder="0" style="border:0"></iframe>
</div>

<?php
    include_once($dir . '/foot.php');
?>
