<?php 

$dir = dirname(__FILE__);

include_once($dir . '/head.php');
include_once($dir . '/mid.php');
?>
<img src="/image/monasteryimage.jpg" width="100%"><br/><br/>
<strong>About</strong>
<br/><br/>
Tisarana is a Myanmar buddhist monastery in Perth, Western Australia. This monastery serves the Myanmar community in Perth and surrounding cities. The monastery has been in existance since 10th January 1993. 
<br/><br/>

<strong>History</strong>
<br/><br/>
By invitation of Myanmar buddhists in Perth, Myaung Mya Sayadaw (The Abbot of Myaung Mya) Ashin Nyanika visited Perth on 8th January 1993. On 10th January 1993, a rented property was annointed as a monastery with an offering of foods to the Abbot and monks. In 1994, Ashin Nyanika visited Perth again and donated AU$12,000 to Tisarana. Using this money as a down payment, a property at 100 Luyer St., Cannington was acquired on 8th April 1994 and used as the monastery. On 22nd February 2004, a new 2.7 acres property at 21 Ashby Close, Forrestfield was acquired moving the monastery to more secluded and spacious lot.
<br/><br/>
Currently Ven. U Thuzana is at service at the monastery.
<?php
    include_once($dir . '/foot.php');
?>
