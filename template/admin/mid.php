<?php

// NOTE: have to use the config var later
if (!isset($page) || $page <= 0)
    $page = 1;

if (!isset($pageSize) || $pageSize <= 0)
    $pageSize = 20;
?>
	<div id='mid'>
		<div id='menu'>
        <!--top in-line menu items-->
            <?php
                $aText = "";
                if (!isset($activeMenu) || empty($activeMenu))
                    $aText .= "class='current'";

                $aText .= " href='/'";
            ?>
                <span><a <?= $aText ?>>home</a></span>  
            <?php
                $aText = "";
                if (isset($activeMenu) && !empty($activeMenu) && "news" == $activeMenu)
                    $aText .= "class='current'";

                $aText .= " href='/news/?page=$page&pageSize=$pageSize'";
            ?>
            <span><a <?= $aText ?>>news</a></span>  
            <?php
                $aText = "";
                if (isset($activeMenu) && !empty($activeMenu) && "event" == $activeMenu)
                    $aText .= "class='current'";

                $aText .= " href='/event/?page=$page&pageSize=$pageSize'";
            ?>
            <span><a <?= $aText ?>>event</a></span>  
            <?php
                $aText = "";
                if (isset($activeMenu) && !empty($activeMenu) && "album" == $activeMenu)
                    $aText .= "class='current'";

                $aText .= " href='#'";
            ?>
            <span><a <?= $aText ?>>album</a></span>  
            <?php
                $aText = "";
                if (isset($activeMenu) && !empty($activeMenu) && "document" == $activeMenu)
                    $aText .= "class='current'";

                $aText .= " href='#'";
            ?>
                <span><a <?= $aText ?>>document</a></span>  
            <?php
                $aText = "";
                if (isset($activeMenu) && !empty($activeMenu) && "discussion" == $activeMenu)
                    $aText .= "class='current'";

                $aText .= " href='#'";
            ?>
            <span><a <?= $aText ?>>discussion</a></span>  
        <div id='content'>
