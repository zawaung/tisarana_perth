<?php 

$dir = dirname(__FILE__);

include_once($dir . '/../head.php');
include_once($dir . '/../mid.php');
?>
<form method='post'>
<input type='hidden' name='a' value='<?php echo ($blank?"add":"update"); ?>' />
<input type='hidden' name='id' value='<?= $news['id']; ?>' />

<div style='padding-bottom: 8px;'>
    <span>Title</span>
    <span>
        <input type='text' style="font-family: Zawgyi-One, sans-serif;width:600px;" value='<?php echo ($blank?"":$news["title"]);?>' name='title' />
    </span>
</div>
<div>
    <span style='vertical-align: top;'>Details</span>
    <span>
        <textarea style="font-family: Zawgyi-One, sans-serif; " cols='80' rows='20' id="details" name='details'><?php echo ($blank?"":$news["details"]); ?></textarea>
    </span>
<div>
<div style='text-align: left;'>
    <br/>
    <input class='cancel-button' type='button'  value='Cancel' />&nbsp;&nbsp;
    <input type='submit' value='<?php echo ($blank?"Save":"Update"); ?>' />
</div>
</form>
<?php
    include_once($dir . '/../foot.php');
?>
