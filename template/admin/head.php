<html>
<head>
    <title>Admin - Tisarana Vihara Monastery</title>
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <style>
	    @import "/css/main.css" screen,print;
        @font-face {
            font-family: 'Gudea-Regular';
            src: url('/font/Gudea-Regular.otf') format('opentype');
        }
        @font-face {
            font-family: 'Gudea-Bold';
            src: url('/font/Gudea-Bold.otf') format('opentype');
        }
        @font-face {
            font-family: 'Zawgyi-One';
            src: url('/font/ZawGyi-One.ttf') format('truetype');
        }
    </style>
    <script src='/javascript/jquery-1.11.1.js' type='application/javascript'></script>
    <script src='/javascript/event_handler.js' type='application/javascript'></script>
</head>
<body>
    <div id="header">
	</div><!-- header -->

