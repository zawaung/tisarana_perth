<?php 

$dir = dirname(__FILE__);

include_once($dir . '/../head.php');
include_once($dir . '/../mid.php');
?>
<table id="list">
    <thead>
        <tr>
            <th>&nbsp;</th>
            <th>Title</th>
            <th>Created On</th>
            <th>Updated On</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan='4'>
                <span><a href='/event/?a=add'>Add Event</a></span>
            </td>
        </tr>
        <?php
            if ($event_list) {
			$i=1;
				foreach ($event_list as $event) {
				$evenOrOdd=($i%2==0?'even':'');
        ?>
            <tr data-id='<?php echo htmlspecialchars($event['id']); ?>' />
            <td>
                <input class='delete-button' type='button' value='x' title='delete' />
            </td>
            <td class='clickable' title='view details to change'>
                <?php echo htmlspecialchars($event['title']); ?>
            </td>
            <td class='clickable' title='view details to change'>
                <?php echo htmlspecialchars($event['createDate']); ?>
            </td>
            <td class='clickable' title='view details to change'>
                &nbsp;
            </td>
        </tr>
        <?php
                $i++;
				}
            }
        ?>
    </tbody>
    <tfoot align="center">
        <tr>
            <td colspan='4'>
                <?php
					echo "Page ";
                    if ($page != 1) {
                        echo "<a href='?page=1&pageSize=$pageSize'>&lt;&lt;</a>&nbsp;&nbsp";
                        echo "<a href='?page=" . ($page - 1) . "&pageSize=$pageSize'>&lt;</a>&nbsp;&nbsp;";
                    }

                    for ($i=1; $i<= $totalPage; $i++) {
                        if ($page == $i)
                            echo "$i&nbsp;";
                        else    
                            echo "<a href='?page=$i&pageSize=$pageSize'>$i</a>&nbsp;";
                    }

                    if ($page < $totalPage) {
                        echo "&nbsp;&nbsp;<a href='?page=" . ($page + 1) . "&pageSize=$pageSize'>&gt;</a>";
                        echo "&nbsp;&nbsp;<a href='?page=$totalPage&pageSize=$pageSize'>&gt;&gt;</a>";
                    }
                ?>
            </td>
        </tr>
    </tfoot>
</table>
<?php
include_once($dir . '/../foot.php');
?>
