<?php 

$dir = dirname(__FILE__);

include_once($dir . '/../head.php');
include_once($dir . '/../mid.php');

$summaryLength = 500;

if ($news_list) {
    foreach ($news_list as $news) {
?>
<div>
    <div class='news-title'>
        <?= htmlentities($news['title']); ?>
    </div>
    <div id='summary-<?= $news["id"]; ?>' class='news-summary'>
        <?php
            $isLong = (mb_strlen($news['details']) > $summaryLength);
            if ($isLong) {
                echo htmlentities(mb_substr($news['details'], 0, $summaryLength) . '.....'); 
            } 
            else {
                echo htmlentities($news['details']); 
            }
        ?>
        <div style='text-align: right;'>
        <?php
        if ($isLong)            
            echo "<input class='more' type='button' value='More' data-id='" . $news["id"] . "'/>";
        else
            echo "&nbsp;";
        ?>
        </div>
    </div>
    <div id='details-<?= $news["id"]; ?>' class='news-details'>
        <?= $news['details']; ?>
        <div style='text-align: right;'>
            <input class='less' type='button' value='Less' data-id='<?= $news["id"]; ?>' />
        </div>
    </div>
</div>
<?php
    }
}

include_once($dir . '/../foot.php');
?>
