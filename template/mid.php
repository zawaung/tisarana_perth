<?php

// NOTE: have to use the config var later
if (!isset($page) || $page <= 0)
    $page = 1;

if (!isset($pageSize) || $pageSize <= 0)
    $pageSize = 20;
?>
	<div id='mid'>
		<div id='menu'>
        <!--top in-line menu items-->
            <?php
                $aText = "";
                if (!isset($activeMenu) || empty($activeMenu))
                    $aText .= "class='current'";

                $aText .= " href='/'";
            ?>
                <span><a <?= $aText ?>>home</a></span>  
            <?php
                $aText = "";
                if (isset($activeMenu) && !empty($activeMenu) && "news" == $activeMenu)
                    $aText .= "class='current'";

                $aText .= " href='/news/?page=$page&pageSize=$pageSize'";
            ?>
            <span><a <?= $aText ?>>news</a></span>  
            <?php
                $aText = "";
                if (isset($activeMenu) && !empty($activeMenu) && "event" == $activeMenu)
                    $aText .= "class='current'";

                $aText .= " href='/event/?page=$page&pageSize=$pageSize'";
            ?>
            <span><a <?= $aText ?>>event</a></span>  
            <?php
                $aText = "";
                if (isset($activeMenu) && !empty($activeMenu) && "album" == $activeMenu)
                    $aText .= "class='current'";

                $aText .= " href='#'";
            ?>
            <span><a <?= $aText ?>>album</a></span>  
            <?php
                $aText = "";
                if (isset($activeMenu) && !empty($activeMenu) && "document" == $activeMenu)
                    $aText .= "class='current'";

                $aText .= " href='#'";
            ?>
                <span><a <?= $aText ?>>document</a></span>  
            <?php
                $aText = "";
                if (isset($activeMenu) && !empty($activeMenu) && "discussion" == $activeMenu)
                    $aText .= "class='current'";

                $aText .= " href='#'";
            ?>
            <span><a <?= $aText ?>>discussion</a></span>  
            <?php
                $aText = "";
                if (isset($activeMenu) && !empty($activeMenu) && "about" == $activeMenu)
                    $aText .= "class='current'";

                $aText .= " href='/about/'";
            ?>
            <span><a <?= $aText ?>>about</a></span>
            <?php
                $aText = "";
                if (isset($activeMenu) && !empty($activeMenu) && "contact" == $activeMenu)
                    $aText .= "class='current'";

                $aText .= " href='/contact/'";
            ?>
            <span><a <?= $aText ?>>contact</a></span>
	    	<span style="border-right:0px;"><input id='searchbox' type='textbox' style="width:98%;" value='enter search'/></span>
			<span style="width:20px;border-right:0px;"><input type="image" value="search" id="searchsubmit" style="align-content:left;vertical-align:middle;" src="/image/go_button_small.png"></span>
        </div>
        <div id='content'>
