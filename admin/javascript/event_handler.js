$(document).ready(function() {
	$('#searchbox')
		.focus(
			function() {
				if ("enter search" == $(this).val().trim().toLowerCase()) {
					$(this).val("");
				}
			}
		)
		.focusout(
			function() {
				if ("" == $(this).val().trim().toLowerCase()) {
					$(this).val("enter search");
				}
			}
		);

   $('td.clickable')
        .click(
            function() {
                id = $(this).parent('tr').data('id'); 
                $(location).attr('href', '?a=view&id=' + id);
            }
        );

   $('input.cancel-button')
	   	.click(
			function() {
				$(location).attr('href', '?');
			}
		);

   $('input.delete-button')
	   	.click(
			function() {
                id = $(this).parent('td').parent('tr').data('id'); 
				$(location).attr('href', '?a=delete&id=' + id);
			}
		);
});
