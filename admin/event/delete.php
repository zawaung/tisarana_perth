<?php

$dir = dirname(__FILE__);

include_once($dir . '/../../conf.php');

$blank = !isset($_GET['id']);

if (!$blank) { 
    $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

    $post_values = http_build_query(array('id' => $id));
    $ch = curl_init();
    $options = array(CURLOPT_URL => WS_URL . "/events/", 
                        CURLOPT_CUSTOMREQUEST => "DELETE", 
                        CURLOPT_POSTFIELDS => $post_values, 
                        CURLOPT_RETURNTRANSFER => true, 
                        CURLOPT_HTTPHEADER => array('Expect:'));

    curl_setopt_array($ch, $options);

    $response = curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);

    $news = json_decode($response, true);

    header('Location: /event/?');
} 


?>
