<?php

$activeMenu = 'event';
$action = '';

switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET' : $action = filter_input(INPUT_GET, 'a', FILTER_SANITIZE_STRING); break;
    case 'POST' : $action = filter_input(INPUT_POST, 'a', FILTER_SANITIZE_STRING); break;
}

switch ($action) {
    case "add": include_once('add.php'); break;
    case "update": include_once('update.php'); break;
    case "view": include_once('view.php'); break;
    case "delete": include_once('delete.php'); break;
    default : include_once('list.php'); break;
}

?>
