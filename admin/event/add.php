<?php

$dir = dirname(__FILE__);

include_once($dir . '/../../conf.php');

$blank = !(isset($_POST['title']) && isset($_POST['details']));
if (!$blank) { 
    $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
    $details = filter_input(INPUT_POST, 'details', FILTER_SANITIZE_STRING);
    
    $post_values = http_build_query(array('title' => $title, 'details' => $details, 'creator' => 99999));
    $ch = curl_init();
    $options = array(CURLOPT_URL => WS_URL . "/events/", 
                        CURLOPT_POST => true, 
                        CURLOPT_POSTFIELDS => $post_values, 
                        CURLOPT_RETURNTRANSFER => true, 
                        CURLOPT_HTTPHEADER => array('Expect:'));

    curl_setopt_array($ch, $options);

    $response = curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);

    $news = json_decode($response, true);

    header('Location: /event/?');
} 

include($dir . '/../../template/admin/event/details.php');

?>
