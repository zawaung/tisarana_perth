<?php

$dir = dirname(__FILE__);
include_once($dir . '/../../conf.php');

$blank = !isset($_POST['id']);

if (!$blank) { 
    $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
    $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
    $details = filter_input(INPUT_POST, 'details', FILTER_SANITIZE_STRING);

    $post_values = http_build_query(array('id' => $id, 'title' => $title, 'details' => $details));
    /*
     * Refere to following address on how to use php://temp memory file in cURL PUT method
     * http://stackoverflow.com/questions/3958226/using-put-method-with-php-curl-library    
     */
    $fp = fopen('php://temp', 'w');
    fwrite($fp, $post_values);
    fseek($fp, 0);

    $ch = curl_init();
    $options = array(CURLOPT_URL => WS_URL . "/news/", 
                        CURLOPT_PUT => true, 
                        CURLOPT_INFILE => $fp, 
                        CURLOPT_INFILESIZE => mb_strlen($post_values),
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_HTTPHEADER => array('Expect:'));


    curl_setopt_array($ch, $options);

    $response = curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);
    fclose($fp);

    $news = json_decode($response, true);
    $cmd = 'view';
} 

header('Location: /news/?');

?>
