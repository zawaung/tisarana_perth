<?php

$dir = dirname(__FILE__);
include_once($dir . '/../../conf.php');

$blank = !isset($_GET['id']);

if (!$blank) { 
    $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

    $ch = curl_init();
    $options = array(CURLOPT_URL => WS_URL . "/news/?id=$id", 
                        CURLOPT_RETURNTRANSFER => true);

    curl_setopt_array($ch, $options);

    $response = curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);

    $news = json_decode($response, true);
    $cmd = 'view';
} 

include($dir . '/../../template/admin/news/details.php');

?>
